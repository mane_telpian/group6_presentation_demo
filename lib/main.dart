import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/animation.dart';
import 'package:matrix_gesture_detector/matrix_gesture_detector.dart';
import 'package:animations/animations.dart';

void main() {runApp(MyApp());}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Group 6 Presentation',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> _screens = [ContainerTransformExamplePage(), ContainerTransformExamplePageContinued(), ProgressIndicatorsExamplePage()];
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      ///FadeThrough Example Using BottomNavigationBar w/ BottomNavigationBarItem
      ///-----------------------------------------------------------------------
      body: PageTransitionSwitcher(
        duration: Duration(milliseconds: 600),
        transitionBuilder: (
            child,
            animation,
            secondaryAnimation,
        ) {
          return FadeThroughTransition(
            fillColor: Colors.pink[900],
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        child: _screens[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blue,
        currentIndex: _selectedIndex,
        onTap: (_Index) {
          setState(() {
          _selectedIndex = _Index;
          });},
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box, color: _selectedIndex == 0 ? Colors.blue[200] : Colors.white,),
            title: Text('Container', style: TextStyle(color: _selectedIndex == 0 ? Colors.blue[200] : Colors.white)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box, color: _selectedIndex == 1 ? Colors.blue[200] : Colors.white,),
            title: Text('3D', style: TextStyle(color: _selectedIndex == 1 ? Colors.blue[200] : Colors.white,)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box, color: _selectedIndex == 2 ? Colors.blue[200] : Colors.white,),
            title: Text('Progress', style: TextStyle(color: _selectedIndex == 2 ? Colors.blue[200] : Colors.white,)),
          ),
        ],
      ),
    );
  }
}

///ContainerTransformExamplePage
///-----------------------------------------------------------------------------
class ContainerTransformExamplePage extends StatefulWidget {
  @override
  _ContainerTransformExamplePageState createState() => _ContainerTransformExamplePageState();
}

class _ContainerTransformExamplePageState extends State<ContainerTransformExamplePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Matrix4 matrix = Matrix4.identity();
  bool scale = false;
  bool translate = false;
  bool rotate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Container Transform'),
          backgroundColor: Colors.blue,
        ),
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///DEFAULT
                ///-------------------------------------------------------------
                MatrixGestureDetector(
                  onMatrixUpdate: (Matrix4 up, Matrix4 down, Matrix4 left, Matrix4 right) {
                    setState(() {
                      matrix = up;
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(border: Border.all(color: Colors.black)),
                    child: Transform(
                      transform: matrix,
                      child: Container(
                        decoration: BoxDecoration(image: DecorationImage(image: AssetImage('images/boo.png'))),
                        width: 100.0,
                        height: 100.0,
                      ),
                    ),
                  ),
                ),

                ///ROTATE
                ///-------------------------------------------------------------
                Center(
                  child: Transform.rotate(
                    angle: rotate ? 1.0 : 0.0,
                    origin: rotate ? Offset(50, 50) : Offset(0, 0),
                    child: Container(
                      child: Center(child: Text('Rotate', style: TextStyle(fontSize: 15, color: Colors.white), )),
                      height: 100.0,
                      width: 100.0,
                      color: Colors.blue[900],
                    ),
                    alignment: Alignment(-0.2, -0.5),
                  ),
                ),
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///SCALE
                ///-------------------------------------------------------------
                Center(
                  child: Transform.scale(
                    scale: scale ? 0.1 : 0.5,
                    child: Container(
                      child: Center(child: Text('Scale', style: TextStyle(fontSize: 30, color: Colors.white), )),
                      height: 200.0,
                      width: 200.0,
                      color: Colors.blue[900],
                    ),
                    alignment: Alignment(-0.4, -0.5),
                  ),
                ),

                ///TRANSLATE
                ///-------------------------------------------------------------
                Center(
                  child: Transform.translate(
                    offset: translate ? Offset(-100.0, -100.0) : Offset(-35, -25),
                    child: Container(
                      child: Center(child: Text('Translate', style: TextStyle(fontSize: 15, color: Colors.white), )),
                      height: 100.0,
                      width: 100.0,
                      color: Colors.blue[900],
                    ),
                  ),
                ),
              ],
            ),

            ///Buttons
            ///-----------------------------------------------------------------
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RawMaterialButton(
                  fillColor: Colors.grey[500],
                  splashColor: Colors.greenAccent,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const <Widget>[
                        Icon(Icons.crop_rotate, color: Colors.white,),
                        SizedBox(width: 10.0,),
                        Text('Rotation', maxLines: 1, style: TextStyle(color: Colors.white)),
                      ],
                    ),
                  ),
                  onPressed: () {setState(() {
                    rotate = !rotate;
                  });},
                  shape: const StadiumBorder(),
                ),
                RawMaterialButton(
                  fillColor: Colors.grey[500],
                  splashColor: Colors.greenAccent,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const <Widget>[
                        Icon(Icons.zoom_in, color: Colors.white,),
                        SizedBox(width: 10.0,),
                        Text('Scale', maxLines: 1, style: TextStyle(color: Colors.white)),
                      ],
                    ),
                  ),
                  onPressed: () {setState(() {
                    scale = !scale;
                  });},
                  shape: const StadiumBorder(),
                ),
                RawMaterialButton(
                  fillColor: Colors.grey[500],
                  splashColor: Colors.greenAccent,
                  child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const <Widget>[
                        Icon(Icons.arrow_forward, color: Colors.white,),
                        SizedBox(width: 10.0,),
                        Text('Translate', maxLines: 1, style: TextStyle(color: Colors.white)),
                      ],
                    ),
                  ),
                  onPressed: () {setState(() {
                    translate = !translate;
                  });},
                  shape: const StadiumBorder(),
                ),
              ],
            ),
          ],
        ),
    );
  }
}

///ContainerTransformExamplePageContinued
///-----------------------------------------------------------------------------
class ContainerTransformExamplePageContinued extends StatefulWidget {
  @override
  _ContainerTransformExamplePageContinuedState createState() => _ContainerTransformExamplePageContinuedState();
}

class _ContainerTransformExamplePageContinuedState extends State<ContainerTransformExamplePageContinued> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  double x = 0;
  double y = 0;
  double z = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Transform Default Constructor'),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Transform(
          transform: Matrix4(
            1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,0,1,
          )..rotateX(x)..rotateY(y)..rotateZ(z),
          alignment: FractionalOffset.center,
          child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                y = y - details.delta.dx / 100;
                x = x + details.delta.dy / 100;
              });
            },
            child: Container(
              decoration: BoxDecoration(image: DecorationImage(image: AssetImage('images/boo.png'))),
              height: 200.0,
              width: 200.0,
            ),
          ),
        ),
      ),
    );
  }
}


///ProgressIndicatorsExamplePage
///-----------------------------------------------------------------------------
class ProgressIndicatorsExamplePage extends StatefulWidget {
  @override
  _ProgressIndicatorsExamplePageState createState() => _ProgressIndicatorsExamplePageState();
}

class _ProgressIndicatorsExamplePageState extends State<ProgressIndicatorsExamplePage> with SingleTickerProviderStateMixin{
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool visible1 = false;
  bool visible2 = false;

  ///Variables for initState
  ///---------------------------------------------------------------------------
  AnimationController _animationController;
  Animation<Color> _colorTween;

  ///InitializeState
  ///Change the colors & duration of the Progress Indicators
  ///---------------------------------------------------------------------------
  void initState() {
    _animationController = AnimationController(
      duration: Duration(milliseconds: 1800),
      vsync: this,
    );
    _colorTween = _animationController.drive(ColorTween(begin: Colors.yellow, end: Colors.green));
    _animationController.repeat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Progress Indicators'),
        backgroundColor: Colors.blue,
      ),
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ///LinearProgressIndicator & CircularProgressIndicator
          ///-------------------------------------------------------------------
          Visibility(
            visible: visible2,
            child: Container(
                child: LinearProgressIndicator(
                  minHeight: 100,
                  backgroundColor: Colors.lightBlueAccent,
                  valueColor: _colorTween,
                )
            ),
          ),
          Visibility(
            visible: visible1,
            child: Container(
              child: CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.lightBlueAccent,
                valueColor: _colorTween,
              )
            ),
          ),

          ///Buttons
          ///-------------------------------------------------------------------
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RawMaterialButton(
                fillColor: Colors.grey[500],
                splashColor: Colors.greenAccent,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.linear_scale, color: Colors.white,),
                      SizedBox(width: 10.0,),
                      Text("Show Linear", maxLines: 1, style: TextStyle(color: Colors.white)),
                    ],
                  ),
                ),
                onPressed: () {setState(() {
                  visible2 = !visible2;
                });},
                shape: const StadiumBorder(),
              ),
              RawMaterialButton(
                fillColor: Colors.grey[500],
                splashColor: Colors.greenAccent,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: const <Widget>[
                      Icon(Icons.loop, color: Colors.white,),
                      SizedBox(width: 10.0,),
                      Text("Show Circular", maxLines: 1, style: TextStyle(color: Colors.white)),
                    ],
                  ),
                ),
                onPressed: () {setState(() {
                  visible1 = !visible1;
                });},
                shape: const StadiumBorder(),
              ),
            ],
          )
        ],
      ),
    );
  }
}